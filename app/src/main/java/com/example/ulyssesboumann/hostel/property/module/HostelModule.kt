package com.example.ulyssesboumann.hostel.module


import com.com.example.ulyssesboumann.hostel.property.view.PropertiesFragment
import com.example.ulyssesboumann.hostel.presenter.HostelEndPoint
import com.example.ulyssesboumann.hostel.presenter.HostelPresenterI
import com.example.ulyssesboumann.hostel.presenter.IHostelPresenter
import com.example.ulyssesboumann.hostel.property.view.PropertiesAdapter
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by ulyssesboumann on 25/03/18.
 */
@Module
class HostelModule(val fragment: PropertiesFragment) {

    @Provides
    @FragmentScope
    fun providePropertiesFragment(): PropertiesFragment {
        return fragment
    }

    @Provides
    @FragmentScope
    fun providePropertiesPresenter(hotelEndPoint: HostelEndPoint): IHostelPresenter {
        return HostelPresenterI(hotelEndPoint)
    }

    @Provides
    @FragmentScope
    fun providePropertiesEndpoint(retrofit: Retrofit): HostelEndPoint {
        return retrofit.create(HostelEndPoint::class.java)
    }

    @Provides
    @FragmentScope
    fun providePropertiesAdaptor(): PropertiesAdapter {
        return PropertiesAdapter(fragment)
    }

}