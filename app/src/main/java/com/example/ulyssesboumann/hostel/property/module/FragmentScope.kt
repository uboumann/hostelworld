package com.example.ulyssesboumann.hostel.module

import javax.inject.Scope

/**
 * Created by ulyssesboumann on 25/03/18.
 */
@Scope
annotation class FragmentScope