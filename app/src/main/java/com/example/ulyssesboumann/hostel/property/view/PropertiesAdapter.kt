package com.example.ulyssesboumann.hostel.property.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.ulyssesboumann.hostel.R
import com.example.ulyssesboumann.hostel.model.Property
import kotlinx.android.synthetic.main.property_item.view.*


/**
 * Created by ulyssesboumann on 25/03/18.
 */
class PropertiesAdapter(callback: Callback) : RecyclerView.Adapter<PropertiesAdapter.ItemPropertyViewHolder>() {

    private var mcallback: Callback? = null
    private var properties: MutableList<Property>? = null
    private var context: Context? = null

    init {
        mcallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemPropertyViewHolder {
        context = parent?.context
        val layoutInflater = LayoutInflater.from(parent?.context)
        return ItemPropertyViewHolder(layoutInflater.inflate(R.layout.property_item, parent, false))
    }
    override fun onBindViewHolder(holder: ItemPropertyViewHolder?, position: Int) {
        val property = properties?.get(position)
        holder?.propertiesName?.text = properties?.get(position)?.getHostelName()
        holder?.propertiesDistance?.text = context!!.getString(R.string.distance, properties?.get(position)?.getDistance()!!.getDistance())
        holder?.propertiesPrice?.text = context!!.getString(R.string.price, convertCoin(properties?.get(position)?.getPrice()!!.getPrice()!!))
        holder?.propertiesRating?.text = context!!.getString(R.string.rating, properties?.get(position)?.getOverall()!!.getRating()!!)
        holder?.propertiesOverview?.text = properties?.get(position)?.getOverview()


        val playerAPicURL = "http://"+properties?.get(position)!!.getImages()!![0].getPrefix()+properties?.get(position)!!.getImages()!![0].getSuffix()

        Glide.with(context).load(playerAPicURL)
                .into(holder?.propertiesImage)


        holder?.itemView?.setOnClickListener({ view ->
            if (property != null) {
                mcallback?.onPropertyClicked(property)
            }
        })
    }

    override fun getItemCount(): Int {
        if(properties != null) {
            return properties?.size!!
        } else
            return 1
    }

    fun updateProperties(properties: MutableList<Property>?) {
        this.properties = properties
    }

    interface Callback {
        fun onPropertyClicked(property: Property)
    }

    class ItemPropertyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var propertiesName = view.titleHostel
        var propertiesDistance = view.distanceValue
        var propertiesPrice = view.value
        var propertiesRating = view.rating
        var propertiesOverview = view.overview

        var propertiesImage = view.imageHostel

    }

    fun convertCoin(value: String):String {
        val price = round(value.toDouble() / 7.55, 2)

        return price.toString()
    }

    fun round(value: Double, places: Int): Double {
        var value = value
        if (places < 0) throw IllegalArgumentException()

        val factor = Math.pow(10.0, places.toDouble()).toLong()
        value = value * factor
        val tmp = Math.round(value)
        return tmp.toDouble() / factor
    }

}
