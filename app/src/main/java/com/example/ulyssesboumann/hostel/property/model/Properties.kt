package com.example.ulyssesboumann.hostel.model

/**
 * Created by ulyssesboumann on 25/03/18.
 */

//@JsonFormat(shape= JsonFormat.Shape.ARRAY)
class Properties {

    private var properties: List<Property>? = null

    fun getProperties(): List<Property>? {
        return properties
    }

    fun setProperties(properties: List<Property>) {
        this.properties = properties
    }

}