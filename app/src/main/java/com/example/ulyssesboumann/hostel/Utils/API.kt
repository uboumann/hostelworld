package com.com.example.ulyssesboumann.hostel.Utils

/**
 * Created by ulyssesboumann on 25/03/18.
 */
enum class API(val value: String) {
    KEY(""),
    TIMEOUT_IN_MS("30000"),
    BASE_URL("https://gist.githubusercontent.com/dovdtel87/ef6dd1422a86554d22172e5975222f81/raw/ba5b81b567efebc1039a481b7e9712b7cd61ea6c/"),
    QUERY_URL("properties.json"),
}