package com.example.ulyssesboumann.hostel.property.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.com.example.ulyssesboumann.hostel.property.view.PropertiesFragment
import com.example.ulyssesboumann.hostel.R
import com.example.ulyssesboumann.hostel.model.Property

/**
 * Created by ulyssesboumann on 25/03/18.
 */
class PropertiesActivity : AppCompatActivity(), PropertiesFragment.Callback {

    val PROPERTIES_FRAGMENT = "PropertiesFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        fragmentManager.beginTransaction()
                .replace(R.id.properties_container, PropertiesFragment(), PROPERTIES_FRAGMENT)
                .addToBackStack(PROPERTIES_FRAGMENT)
                .commit()
    }

    override fun onPropertiesLoaded(property: Property) {

    }

    override fun onPropertiesClicked(property: Property) {
        loadPropertiesDetailsFragment(property)
    }

    private fun loadPropertiesDetailsFragment(property: Property) {

    }
}