package com.com.example.ulyssesboumann.hostel.Utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import com.google.gson.Gson
import com.example.ulyssesboumann.hostel.model.Properties
import com.example.ulyssesboumann.hostel.model.Property
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by ulyssesboumann on 25/03/18.
 */
class DeserializerJsonProperties<T> : JsonDeserializer<Properties> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Properties {

        val properties = Properties()

                Observable.just(json.toString())
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map ({ elements ->
                             Gson().fromJson(elements, arrayOf(Property())::class.java)
                             })
                        .flatMap({
                            jsonObjects -> Observable.fromArray(jsonObjects)
                        })
                        .blockingSubscribe({
                            listOfProperties -> properties.setProperties(listOfProperties.toList())
                        })

        return properties
    }
}