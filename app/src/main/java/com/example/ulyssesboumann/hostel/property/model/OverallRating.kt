package com.example.ulyssesboumann.hostel.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ulyssesboumann on 25/03/18.
 */
class OverallRating {
    @SerializedName("overall")
    @Expose
    private var overall: String? = null

    fun getRating(): String? {
        return overall
    }
}