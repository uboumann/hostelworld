package com.example.ulyssesboumann.hostel.property.view

import com.example.ulyssesboumann.hostel.model.Property

/**
 * Created by ulyssesboumann on 25/03/18.
 */
interface PropertyView {

    fun showProperties(properties: List<Property>)

    fun loadingStarted()

    fun loadingFailed(errorMessage: String?)

    fun onPropertyClicked(property: Property)
}