package com.example.ulyssesboumann.hostel.presenter

import com.example.ulyssesboumann.hostel.property.view.PropertyView


/**
 * Created by ulyssesboumann on 25/03/18.
 */
interface IHostelPresenter {

    fun displayProperties()

    fun setView(view: PropertyView)

    fun destroy()
}