package com.example.ulyssesboumann.hostel.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ulyssesboumann on 25/03/18.
 */

class Price {
    @SerializedName("value")
    @Expose
    private var value: String? = null

    fun getPrice(): String? {
        return value
    }
}