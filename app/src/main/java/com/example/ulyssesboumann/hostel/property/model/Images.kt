package com.example.ulyssesboumann.hostel.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ulyssesboumann on 25/03/18.
 */

class Images {

    @SerializedName("prefix")
    @Expose
    private var prefix: String? = null
    @SerializedName("suffix")
    @Expose
    private var suffix: String? = null

    fun getPrefix(): String? {
        return prefix
    }

    fun getSuffix(): String? {
        return suffix
    }

}