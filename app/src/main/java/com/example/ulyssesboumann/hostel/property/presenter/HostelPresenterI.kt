package com.example.ulyssesboumann.hostel.presenter


import com.com.example.ulyssesboumann.hostel.Utils.API
import com.example.ulyssesboumann.hostel.model.Properties
import com.example.ulyssesboumann.hostel.property.view.PropertyView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


/**
 * Created by ulyssesboumann on 25/03/18.
 */
class HostelPresenterI(hostelEndPoint: HostelEndPoint) : IHostelPresenter {


    //@Inject
   private var view: PropertyView? = null
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
   // @Inject
    var propertiesEndpoint: HostelEndPoint = hostelEndPoint

    override fun displayProperties() {

        val disposableProperties: Disposable = propertiesEndpoint?.fetchProperties(API.QUERY_URL.value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe({
                    if (isViewAttached()) {
                        view?.loadingStarted()
                    }
                }).subscribeBy(
                    onNext = {
                        properties -> onPropertiesResult(properties)
                    },
                    onError = {
                        error -> onPropertiesFetchError(error)
                    },
                    onComplete = {}
        )

        disposableProperties?.let { compositeDisposable?.add(it) }
    }

    override fun setView(view: PropertyView) {
        this.view = view
    }

    override fun destroy() {
        view = null
        compositeDisposable?.clear()
    }

    private fun onPropertiesResult(properties: Properties) {

        val _properties = properties?.getProperties()

        if (isViewAttached()) {
            if (_properties != null) {
                view?.showProperties(_properties)
            }
        }
    }

    private fun onPropertiesFetchError(throwable: Throwable) {
        if (isViewAttached()) {
            view?.loadingFailed(throwable.message)
        } else {
            // do nothing
        }
    }

    private fun isViewAttached(): Boolean {
        return view != null
    }
}