package com.com.example.ulyssesboumann.hostel.property.view

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ulyssesboumann.hostel.App
import com.example.ulyssesboumann.hostel.R
import com.example.ulyssesboumann.hostel.model.Property
import com.example.ulyssesboumann.hostel.module.HostelModule
import com.example.ulyssesboumann.hostel.presenter.IHostelPresenter
import com.example.ulyssesboumann.hostel.property.view.PropertiesAdapter
import com.example.ulyssesboumann.hostel.property.view.PropertyView

import javax.inject.Inject

/**
 * Created by ulyssesboumann on 25/03/18.
 */
class PropertiesFragment : Fragment(), PropertyView, PropertiesAdapter.Callback {

    @Inject
    lateinit var propertiesPresenterI: IHostelPresenter
    @Inject
    lateinit var adapter: PropertiesAdapter

    val component by lazy {
        App.get(activity).appComponent.plus(HostelModule(this))
    }

    private var properties = mutableListOf<Property>()
    private var callback: Callback? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as Callback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        component.inject(this)
        propertiesPresenterI?.setView(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater?.inflate(R.layout.fragment_properties, container, false)
        rootView?.let { initLayoutReferences(it) }!!
        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        propertiesPresenterI?.displayProperties()
    }

    private fun initLayoutReferences(rootView: View) {

        val rvProperties = rootView.findViewById<View>(R.id.recyclerViewProperties) as RecyclerView
        rvProperties.setHasFixedSize(true)

        rvProperties.layoutManager = LinearLayoutManager(activity)
            this.adapter?.updateProperties(this.properties)
            rvProperties.setAdapter(this.adapter)
    }

    override fun showProperties(properties: List<Property>) {
        this.properties?.clear()
        this.properties?.addAll(properties)
        this.adapter?.notifyDataSetChanged()
    }

    override fun loadingStarted() {
        Toast.makeText(activity, "loading properties", Toast.LENGTH_SHORT).show()
    }

    override fun loadingFailed(errorMessage: String?) {
        Toast.makeText(activity, "failed loading properties" + errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onPropertyClicked(property: Property) {
        callback?.onPropertiesClicked(property)
        Toast.makeText(activity, "Sorry have no time to implement properties details", Toast.LENGTH_LONG).show()
    }

    public interface Callback {
        fun onPropertiesLoaded(property: Property)
        fun onPropertiesClicked(property: Property)
    }
}

