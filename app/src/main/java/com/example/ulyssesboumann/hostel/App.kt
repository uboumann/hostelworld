package com.example.ulyssesboumann.hostel

import android.app.Application
import android.content.Context
import android.os.StrictMode
import com.example.ulyssesboumann.hostel.components.AppComponent
import com.example.ulyssesboumann.hostel.components.DaggerAppComponent
import com.example.ulyssesboumann.hostel.modules.AppModule
import com.example.ulyssesboumann.hostel.modules.NetworkModule

/**
 * Created by ulyssesboumann on 25/03/18.
 */
class App: Application() {

    companion object {
        fun get(context: Context): App {
            return context.applicationContext as App
        }
    }

    val appComponent: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule())
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}