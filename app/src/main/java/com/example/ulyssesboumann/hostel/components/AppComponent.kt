package com.example.ulyssesboumann.hostel.components

import com.example.ulyssesboumann.hostel.App
import com.example.ulyssesboumann.hostel.component.PropertiesComponent
import com.example.ulyssesboumann.hostel.module.HostelModule
import com.example.ulyssesboumann.hostel.modules.AppModule
import com.example.ulyssesboumann.hostel.modules.NetworkModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by ulyssesboumann on 25/03/18.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {
    fun inject(app: App)
    fun plus(hostelModule: HostelModule): PropertiesComponent
}