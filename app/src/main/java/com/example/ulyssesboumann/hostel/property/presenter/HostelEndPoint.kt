package com.example.ulyssesboumann.hostel.presenter

import com.example.ulyssesboumann.hostel.model.Properties
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by ulyssesboumann on 25/03/18.
 */
interface HostelEndPoint {

    @GET
    fun fetchProperties(@Url url: String): Flowable<Properties>
}