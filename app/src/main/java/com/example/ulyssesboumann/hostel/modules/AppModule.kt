package com.example.ulyssesboumann.hostel.modules

import android.content.Context
import com.example.ulyssesboumann.hostel.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ulyssesboumann on 25/03/18.
 */
@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideContext(): Context {
        return app
    }
}