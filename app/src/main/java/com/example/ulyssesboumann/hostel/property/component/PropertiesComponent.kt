package com.example.ulyssesboumann.hostel.component


import com.com.example.ulyssesboumann.hostel.property.view.PropertiesFragment
import com.example.ulyssesboumann.hostel.module.FragmentScope
import com.example.ulyssesboumann.hostel.module.HostelModule
import dagger.Subcomponent

/**
 * Created by ulyssesboumann on 25/03/18.
 */
@FragmentScope
@Subcomponent(modules = arrayOf(HostelModule::class))
interface PropertiesComponent {
    fun inject(propertiesFragment: PropertiesFragment)
}