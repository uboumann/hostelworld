package com.example.ulyssesboumann.hostel.model

/**
 * Created by ulyssesboumann on 25/03/18.
 */
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ulyssesboumann on 25/03/18.
 */


class Property {
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("distance")
    @Expose
    private var distance: Distance? = null
    @SerializedName("lowestPricePerNight")
    @Expose
    private var price: Price? = null

    @SerializedName("overallRating")
    @Expose
    private var overallRating: OverallRating? = null

    private var images: List<Images>? = null

    @SerializedName("overview")
    @Expose
    private var overview: String? = null

    fun getHostelName(): String? {
        return name
    }

    fun getDistance(): Distance? {
        return distance
    }

    fun getPrice(): Price? {
        return price
    }

    fun getOverall(): OverallRating? {
        return overallRating
    }

    fun getImages(): List<Images>? {
        return images
    }

    fun getOverview(): String? {
        return overview
    }

}